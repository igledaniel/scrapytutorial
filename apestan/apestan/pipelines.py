# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from textblob import TextBlob

class ApestanPipeline(object):
    def process_item(self, item, spider):
        return item


class SentimentPipeline(object):
    def process_item(self, item, spider):
        for text in item['comments']:
            blob = TextBlob(text["comment"])
            text["sentiment"] = blob.sentiment.polarity
        return item
