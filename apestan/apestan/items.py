# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class ApestanItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    case = scrapy.Field()
    title = scrapy.Field()
    stars = scrapy.Field()
    opinions = scrapy.Field()
    date = scrapy.Field()
    text = scrapy.Field()
    comments = scrapy.Field()
    sentiment = scrapy.Field()
