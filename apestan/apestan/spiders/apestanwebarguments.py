# -*- coding: utf-8 -*-
import scrapy
import re
import datetime
from apestan.items import ApestanItem


class ApestanwebargumentsSpider(scrapy.Spider):
    name = "apestanwebarguments"
    allowed_domains = ["apestan.com"]

    def __init__(self, url='', domain=None):
        self.start_urls = [url]
        self.domain = domain

    def parse(self, response):
        #print  response.xpath('//body//text()').extract()

         spans = response.xpath('//div[@class="case_details"]/div/span/text()').extract()
         headers = response.xpath('//div[@class="case_details"]/div[contains(@class,"border_bottom")]/span/text()').extract()
         case = headers[0]
         stats = (re.findall('\d+', headers[1] ))
         stars = int(stats[0])
         opinions = int(stats[1])
         datestring = response.xpath('//div[@class="case_details"]/div/span[@class="right"]/a/text()').extract()
         date = datetime.datetime(int('20' + datestring[2]),int(datestring[1]),int(datestring[0]))
         bodytext = response.xpath('//div[@class="text-block"]/span/text()').extract()
         text = ' '.join(bodytext)
         comments = []

         for comment in response.xpath('//div[contains(@class,"light_grey")]')[1:]:

             username = comment.xpath('div/span[@class="user-name"]/strong/text()').extract()
             thumbdown = comment.xpath('div/div/span/span[@class="red"]/text()').extract()
             usergradedown = comment.xpath('div/div/div/span[contains(@class,"gradePoints")]/text()').extract()
             usergradeall = comment.xpath('div/div/span/span[@class="green"]/text()').extract()
             commentuser = comment.xpath('following-sibling::div[contains(@class,"wrap_comment")]/text()').extract()
             comments.append({'username': username[0], 'comment': ' '.join(commentuser), 'thumbdown':thumbdown[0], 'usergradedown':usergradedown[0],
             'usergradeall':usergradeall[0]})


         item = ApestanItem()
         item['case'] = case
         item ['stars'] = stars
         item['opinions'] = opinions
         item['date'] = date
         item['text'] = text
         item['comments'] = comments
         return item
